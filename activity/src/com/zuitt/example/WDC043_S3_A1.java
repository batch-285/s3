package com.zuitt.example;
import java.util.Scanner;

public class WDC043_S3_A1 {
    public static void main(String[] args) {
        Scanner factorial = new Scanner(System.in);
        int num;
        System.out.println("Input an integer whose factorial will be computed:");

        while (true) {
            try {
                num = factorial.nextInt();
                if (num < 1) {
                    if (num == 0) {
                        System.out.println("The factorial of 0 is 1. Please enter a positive integer:");
                    } else {
                        System.out.println("Invalid input! Please enter a positive integer:");
                    }
                } else {
                    break;
                }
            } catch (Exception e) {
                System.out.println("Invalid input! Please enter an integer:");
                factorial.next();
            }
        }

        int answer = 1;
        for (int i = 1; i <= num; i++) {
            answer *= i;
        }
        System.out.println("The factorial of " + num + " is " + answer);
    }
}
