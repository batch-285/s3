package com.zuitt.example;

import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int num = 0;
        System.out.println("Enter a number:");

        try {
            num = input.nextInt();
        } catch (Exception e) {
            System.out.println("Invalid Input!");
            e.printStackTrace();
        } finally {
            System.out.println("finally: " + num);
        }

        System.out.println("hello");
    }
}
