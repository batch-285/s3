package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class RepetitionControl {
    public static void main(String[] args) {
        int x =0;
        while (x < 3) {
            System.out.println("While loop: " + x);
            x++;
        }

        int y = 3;
        do {
            System.out.println("Do while loop: " + y);
        } while (y > 3);

        for (int i = 0; i < 3; i++) {
            System.out.println("For loop: " + i);
        }

        int[] arr = {100, 200, 300};
        for (int i = 0; i < arr.length; i++) {
            System.out.println("Array For loop: " + arr[i]);
        }

        String[] bandArr = {"John", "Po", "George"};
        for (String member: bandArr) {
            System.out.println("For Each loop: " + member);
        }

        String[][] classroom = new String[3][3];
        //[row][column]

        //First row
        classroom[0][0] = "Jenny";
        classroom[0][1] = "Liza";
        classroom[0][2] = "Rose";
        //Second row
        classroom[1][0] = "Ash";
        classroom[1][1] = "Misty";
        classroom[1][2] = "Brock";
        //Third row
        classroom[2][0] = "Amy";
        classroom[2][1] = "Lulu";
        classroom[2][2] = "Morgan";

        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                System.out.println("Multiple array int: ["+ row +"] ["+ col +"] = " + classroom[row][col]);
            }
        }

        for (String[] row: classroom) {
            for (String col: row) {
                System.out.println("Multiple array String: " + col);
            }
        }

        for (int row = 0; row < classroom.length; row++) {
            for (int col = 0; col < classroom[row].length; col++) {
                System.out.println("Matrix: " + classroom[row][col]);
            }
            System.out.println();
        }

        //mini activity
        for (int i = 1; i <= 3; i++) {
            for (int j = 1; j <= 3; j++) {
                System.out.print((i == 1 || i == 3) ? "* " : (j == 1 ? "2 " : (j == 2 ? "8 " : "5 ")));
            }
            System.out.println();
        }

        ArrayList<Integer> num = new ArrayList<>();
        num.add(5);
        num.add(10);
        num.add(15);
        System.out.println("Arraylist:" + num);
        num.forEach(number -> System.out.println("Arraylist for each:" + number));

        HashMap<String, Integer> grades = new HashMap<>() {
            {
                this.put("Math", 90);
                this.put("English", 95);
                this.put("Science", 97);
            }
        };

        System.out.println("Hashmaps:" + grades);
        grades.forEach((subj, grade) -> System.out.println("Hashmaps for each: " + subj + " > " + grade));

    }
}
